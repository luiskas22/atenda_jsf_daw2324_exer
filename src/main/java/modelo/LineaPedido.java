package modelo;

public class LineaPedido {
	private int id;
	private Produto produto;
	private int unidades;
	private int desconto;
	private double prezo;
	private double coste;
	public LineaPedido() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public int getUnidades() {
		return unidades;
	}
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	public int getDesconto() {
		return desconto;
	}
	public void setDesconto(int desconto) {
		this.desconto = desconto;
	}
	public double getPrezo() {
		return prezo;
	}
	public void setPrezo(double prezo) {
		this.prezo = prezo;
	}
	public double getCoste() {
		return coste;
	}
	public void setCoste(double coste) {
		this.coste = coste;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LineaPedido other = (LineaPedido) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "LineaPedido [id=" + id + ", produto=" + produto + ", unidades=" + unidades + ", desconto=" + desconto
				+ "]";
	}

	
}
