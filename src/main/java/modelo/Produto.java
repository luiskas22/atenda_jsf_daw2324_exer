package modelo;


import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Named;



@Named
@Dependent
public class Produto implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String nome;
	private double prezo;
	private int desconto;
	private double coste;
	private int iva;
	private int stock;
	private String foto;
	private boolean baixa;

	public Produto() {
		super();

	}
	public Produto(int id, String nome, double prezo, int stock) {
		super();
		this.id=id;
		this.nome = nome;
		this.prezo = prezo;
		this.stock = stock;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getPrezo() {
		return prezo;
	}
	public void setPrezo(double prezo) {
		this.prezo = prezo;
	}
	public int getDesconto() {
		return desconto;
	}
	public void setDesconto(int desconto) {
		this.desconto = desconto;
	}
	public double getCoste() {
		return coste;
	}
	public void setCoste(double coste) {
		this.coste = coste;
	}
//	public TipoIVA getIva() {
//		return iva;
//	}
	public int getIva() {
		return iva;
	}
	public void setIva(int iva) {
		this.iva = iva;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public boolean isBaixa() {
		return baixa;
	}
	public void setBaixa(boolean baixa) {
		this.baixa = baixa;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Produto [id=" + id + ", nome=" + nome + ", prezo=" + prezo + ", desconto=" + desconto + ", coste="
				+ coste + ", iva=" + iva + ", stock=" + stock + ", foto=" + foto + ", baixa=" + baixa + "]";
	}




}
